# SF-Coding-Exercise #

### Purpose ###

* Coding exercises for SF

### Contents ###

* SF_Coding_EX_01: Simple test case to verify name field is blank when form is loaded.
* SF_Coding_EX_02: negative test case to verify field validation message is displayed to the user when attempting to submit an incomplete form.